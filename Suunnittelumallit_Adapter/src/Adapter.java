
public class Adapter implements MediaPlayer{
	
	private MP4Player mp4player;

	@Override
	public void play(String audioType, String filename) {
		if (audioType.equalsIgnoreCase("mp4")) {
			mp4player = new MP4Player();
			mp4player.playVideo(filename);
		}
		
	}

	@Override
	public void storeAudio(String audioType, String filename) {
		if (audioType.equalsIgnoreCase("mp4")) {
			mp4player = new MP4Player();
			mp4player.StoreData(filename);
		}
		
	}

}
