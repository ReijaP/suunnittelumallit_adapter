
public class MP3Player extends MP4Player implements MediaPlayer {
	
	private Adapter adapter;
	
	@Override
	public void play(String audioType, String filename) {
		if (audioType.equalsIgnoreCase("mp4")) {
			adapter = new Adapter();
			adapter.play(audioType, filename);
		}
		else {
			System.out.println("MP3-class");
			System.out.println("Playing " + audioType + " file, named " + filename);
		}
		
	}

	@Override
	public void storeAudio(String audioType, String filename) {
		if (audioType.equalsIgnoreCase("mp4")) {
			adapter = new Adapter();
			adapter.storeAudio(audioType, filename);
		}
		else {
			System.out.println("MP3-class");
			System.out.println("Saving " + audioType + " file, named " + filename);
		}
		
	}

}
