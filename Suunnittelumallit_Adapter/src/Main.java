
public class Main {

	public static void main(String[] args) {
		
		MediaPlayer mediaplayer = new MP3Player();
		
		mediaplayer.storeAudio("mp3", "Sweet home Alabama");
		mediaplayer.storeAudio("mp4", "Home Alone 2");
		mediaplayer.play("mp3", "Song");
		mediaplayer.play("mp4", "Movie");

	}

}
