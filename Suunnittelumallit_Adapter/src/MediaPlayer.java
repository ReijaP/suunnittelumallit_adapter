
public interface MediaPlayer {
	
	public void play(String audioType, String filename);
	public void storeAudio(String audioType, String filename);

}
